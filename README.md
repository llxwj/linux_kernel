#linux_kernel

| 目录                              | 说明                                    |
| ------------------------------- | ------------------------------------- |
| alloc_memory_use_negative_index | linux内核中vmalloc动态内存用负数索引访问内存造成死机的测试代码 |
| file                            | 从网上搜索整理的内核文件操作测试例子.                   |
| signal                          | 内核态与用户态之间通过信号进行通信                     |
| module_parameter                | 内核模块的参数                               |

