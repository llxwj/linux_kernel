#linux_kernel_alloc_memory_use_negative_index

我们的软件中需要分配较大的动态内存,Redhat 6.5的2.6.32的内核默认kmalloc能分配的
最大内存为4MB,而我们的需要的内存比这4MB还要大,所以使用了vmalloc来分配内存,但是
访问这个内存的索引我们有个特殊的用法,某一个时刻, 这个内存索引会为负值, 当用这
个负数内存索引去访问内存的时候, 内核驱动模块直接引起系统死机,kdump启动后,保留
下的dmesg信息主要为:

```
<1>BUG: unable to handle kernel paging request at ffffc90001b74ffc
<1>IP: [<ffffffffa0128080>] negative_index_init+0x80/0x9f [negative_index]
<4>PGD 11feab067 PUD 11feac067 PMD 115e5d067 PTE 0
<4>Oops: 0000 [#1] SMP
<4>last sysfs file: /sys/kernel/mm/ksm/run
```

造成页请求失败, 为了验证这个问题,特此写了此测试代码, 死机具体原因不详,以待进一步解决验证.
