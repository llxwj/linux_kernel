#include <linux/init.h>
#include <linux/module.h>
#include <linux/blkdev.h>
#include <linux/mm.h>

static void negative_index_exit(void)
{
	printk(KERN_ERR "negative index exit.\n");
}

static int __init negative_index_init(void)
{
	int* mem_kmalloc = NULL;
	int* mem_kzalloc = NULL;
	int* mem_vmalloc = NULL;

	printk(KERN_ERR "negative index init.\n");

	mem_kmalloc = kmalloc(64 * 1024, GFP_KERNEL);
	if(mem_kmalloc == NULL)
	{
		printk(KERN_ERR "kmalloc failed.\n");
		return 0;
	}

	printk(KERN_ERR "print mem_kmalloc element at index -1:%d\n", mem_kmalloc[-1]);
	kfree(mem_kmalloc);

	mem_kzalloc = kzalloc(128 * 1024, GFP_KERNEL);
	if(mem_kzalloc == NULL)
	{
		printk(KERN_ERR "kzalloc failed.\n");
		return 0;
	}

	printk(KERN_ERR "print mem_kzalloc element at index -1:%d\n", mem_kzalloc[-1]);
	kfree(mem_kzalloc);

	mem_vmalloc = vmalloc(5 * 1024 * 1024);
	if(mem_vmalloc == NULL)
	{
		printk(KERN_ERR "vmalloc failed.\n");
		return 0;
	}

	printk(KERN_ERR "print mem_vmalloc element at index -1:%d", mem_vmalloc[-1]);
	vfree(mem_vmalloc);

	return 0;
}

module_init(negative_index_init);
module_exit(negative_index_exit);

