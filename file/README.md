#linux_kernel_file

从网络上搜索整理的linux内核空间文件操作的示例代码,在linux 2.6.32上测试过.

| 功能 | 函数原型 |
| ---:| -------:|
| 打开文件| struct file *filp_open(const char* filename, int flags, int mode) |
| 读取文件 | ssize_t vfs_read(struct file *file, char __user *buf, size_t count, loff_t *pos) |
| 写文件 | ssize_t vfs_write(struct file *file, const char __user *buf, size_t count, loff_t *pos) |
| 关闭文件 | int filp_close(struct file *filp, fl_owner_t id) |

有的时候执行会报异常
```
------------[ cut here ]------------
WARNING: at fs/namei.c:1552 lookup_one_len+0xf1/0x110() (Not tainted)
Hardware name: VirtualBox
Modules linked in: kernel_file(+)(U) des_generic ecb md4 nls_utf8 cifs ebtable_nat ebtables ipt_MASQUERADE iptable_nat nf_nat xt_CHECKSUM iptable_mangle bridge autofs4 8021q garp st
p llc vboxsf(U) ipt_REJECT nf_conntrack_ipv4 nf_defrag_ipv4 iptable_filter ip_tables ip6t_REJECT nf_conntrack_ipv6 nf_defrag_ipv6 xt_state nf_conntrack ip6table_filter ip6_tables ip
v6 vhost_net macvtap macvlan tun uinput microcode sg i2c_piix4 i2c_core snd_intel8x0 snd_ac97_codec ac97_bus snd_seq snd_seq_device snd_pcm snd_timer snd soundcore snd_page_alloc vb
oxguest(U) e1000 ext4 jbd2 mbcache sd_mod crc_t10dif sr_mod cdrom video output ahci pata_acpi ata_generic ata_piix dm_mirror dm_region_hash dm_log dm_mod [last unloaded: kernel_file
]
Pid: 5440, comm: insmod Not tainted 2.6.32-431.el6.x86_64 #1
Call Trace:
 [<ffffffff81071e27>] ? warn_slowpath_common+0x87/0xc0
 [<ffffffff81071e7a>] ? warn_slowpath_null+0x1a/0x20
 [<ffffffff81196f41>] ? lookup_one_len+0xf1/0x110
 [<ffffffffa014f060>] ? file_remove.clone.0+0x60/0x90 [kernel_file]
 [<ffffffff8118a865>] ? fput+0x25/0x30
 [<ffffffff81185b8d>] ? filp_close+0x5d/0x90
 [<ffffffffa01510df>] ? init_module+0xdf/0xec [kernel_file]
 [<ffffffffa0151000>] ? init_module+0x0/0xec [kernel_file]
 [<ffffffff8100204c>] ? do_one_initcall+0x3c/0x1d0
 [<ffffffff810bc531>] ? sys_init_module+0xe1/0x250
 [<ffffffff8100b072>] ? system_call_fastpath+0x16/0x1b
---[ end trace 484d1bd493c509c0 ]---
[root@rhel216 kernel_file]#
```
